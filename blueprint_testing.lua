-- Watch for events for blueprint(book) items, then evaluate those.

function eval_player_blueprints(player_index, iterations)
	local player = game.get_player(player_index)
	local print = player.print
	local inventory = player.get_inventory(defines.inventory.player_main)
	print("DEBUG: Finding Blueprints in inventory...")
	log("DEBUG: Finding Blueprints in inventory...")
	for idx = 1, #inventory do
		local stack = inventory[idx]
		if stack and stack.valid and stack.valid_for_read then
			if stack.is_blueprint or stack.is_blueprint_book then
				local profiler = game.create_profiler()
				profiler.reset()
				for jdx = 1, iterations do
					result = stack.export_stack()
				end
				profiler.stop()
				local pl2 = game.create_profiler()
				pl2.reset()
				for jdx = 1, iterations do
					stack.import_stack(result)
				end
				pl2.stop()
			
				--print({'', "Found blueprint to export, ", stack.item_number, " ", stack.label})
				local out = 
				{
					'', "BP IO (", stack.item_number, ") (x", iterations, "): ",
					"\tExport: ", profiler,
					"\tImport: ", pl2,
					"\tSize: ", #result
				}
				print(out)
				log(out)
			end
		end
	end
end

commands.add_command('profile_bps', "Profile the blueprints in your inventory.", function(event)
	if event.player_index > 0 then
		local iterations = 1
		if event.parameter then
			iterations = tonumber(event.parameter)
		end
		eval_player_blueprints(event.player_index, iterations)
	else
		game.print("Unable to evaluate server's inventory.")
	end
end)
