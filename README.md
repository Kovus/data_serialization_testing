# data_serialization_testing

Testing various methods of getting data out of Factorio lua tables into an exported format, as well as getting it back in.

I wanted to know what the performance was for doing something like messagepack (which can lead to very compact messaging when compared to json in some cases) when used in Factorio.  The catch of course is that I'm largely interested in it regarding using data that be sent via rcon.  My understanding is that rcon.print will stop when it reaches a NULL byte, and messagepack is certainly likely to have those.  Which lead to messagepack+base64 encoding.  Then I learned how to use item-with-tags to use some built-in export content, so that also got added.

Everything has good and bad parts, but since this is all about having a generic lua table export/import functionality, no "build-it-myself" binary designs are included here.

### What I learned.....

So, a long time ago, before the Factorio `table_to_json` and `json_to_table` functions were added, I was using dkjson to export data.  It turns out that the built-in JSON is 3x faster at converting to json, and 9x faster at converting from json.  I'm glad that they added that!

With some more recent code, I was actually doing some strange things to generate an export string that looked similar to what Factorio uses for it's bluepring and map exports.  I wish that the operation would be directly available instead of having to abuse an `item-with-tags` entity/item thing, but that also turns out to be pretty simple code.  So I might start using that.

If we take the built-in JSON operations as a basline, then we can see that abusing item-with-tags is only about 40% slower, but still substantially faster than trying to use a pure-lua implementation of messagepack, and beats the pants off of using either dkjson or doing base64 encoding of messagepack results (which is needed for exporting that data via rcon).

![](Sample.png)
