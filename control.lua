require 'blueprint_testing'

sample = require 'testdata'

mp = require 'MessagePack'
b64 = require 'ee5_base64'
dkjson = require 'dkjson'

-- Serpent & load

function export_serpent(data, iterations)
	local result = nil
	for idx = 1, iterations do
		result = serpent.block(data)
	end
	return result
end

function import_loadstring(luadata, iterations)
	local data = export_serpent(luadata, 1)
	-- loadstring, here we come!
	local result = nil
	for idx = 1, iterations do
		result = loadstring(data)
	end
	return result
end

-- Factorio's builtin JSON

function export_builtin_json(data, iterations)
	local result = nil
	for idx = 1, iterations do
		result = game.table_to_json(data)
	end
	return result
end

function import_builtin_json(luadata, iterations)
	local data = export_builtin_json(luadata, 1)
	local result = nil
	for idx = 1, iterations do
		result = game.json_to_table(data)
	end
	return result
end

-- External JSON library, dkjson

function export_dkjson(data, iterations)
	local result = nil
	for idx = 1, iterations do
		result = dkjson.encode(data)
	end
	return result
end

function import_dkjson(luadata, iterations)
	local data = export_dkjson(luadata, 1)
	local result = nil
	for idx = 1, iterations do
		result = dkjson.decode(data)
	end
	return result
end

-- Pure-lua MessagePack library.

function export_msgpack(data, iterations)
	local result = nil
	for idx = 1, iterations do
		result = mp.pack(data)
	end
	return result
end

function import_msgpack(luadata, iterations)
	local data = export_msgpack(luadata, 1)
	local result = nil
	for idx = 1, iterations do
		result = mp.unpack(data)
	end
	return result
end

-- Pure-lua MessagePack + pure-lua base64

function export_msgpack_b64(data, iterations)
	local result = nil
	for idx = 1, iterations do
		result = b64.encode(mp.pack(data))
	end
	return result
end

function import_msgpack_b64(luadata, iterations)
	local data = export_msgpack_b64(luadata, 1)
	local result = nil
	for idx = 1, iterations do
		result = mp.unpack(b64.decode(data))
	end
	return result
end

-- Creating an itemstack, assigning a tag, export, destroy itemstack.

function export_item_with_tags(data, iterations)
	local result = nil
	for idx = 1, iterations do
		local item = game.surfaces[1].create_entity({
			name="item-on-ground",
			position={0,0},
			stack='item-with-tags',
		})
		item.stack.set_tag('profile', data)
		result = item.stack.export_stack()
		item.destroy()
	end
	return result
end

function import_item_with_tags(luadata, iterations)
	local data = export_persist_item_with_tags(luadata, 1)
	local result = nil
	for idx = 1, iterations do
		local item = game.surfaces[1].create_entity({
			name="item-on-ground",
			position={0,0},
			stack='item-with-tags',
		})
		item.stack.import_stack(data)
		result = item.stack.get_tag('profile')
		item.destroy()
	end
	return result
end

-- Create single itemstack, assigning tag (over iterations), export, destroy.

function export_persist_item_with_tags(data, iterations)
	local result = nil
	local item = game.surfaces[1].create_entity({
		name="item-on-ground",
		position={0,0},
		stack='item-with-tags',
	})
	for idx = 1, iterations do
		item.stack.set_tag('profile', data)
		result = item.stack.export_stack()
	end
	item.destroy()
	return result
end

function import_persist_item_with_tags(luadata, iterations)
	local data = export_persist_item_with_tags(luadata, 1)
	local result = nil
	local item = game.surfaces[1].create_entity({
		name="item-on-ground",
		position={0,0},
		stack='item-with-tags',
	})
	for idx = 1, iterations do
		item.stack.import_stack(data)
		result = item.stack.get_tag('profile')
	end
	item.destroy()
	return result
end

-- Profiling performance.

function profile(method, params, friendly_name)
	local iterations = 1
	local print = game.print
	if params.player_index > 0 then
		print = game.get_player(params.player_index).print
	end
	if params.parameter then
		iterations = tonumber(params.parameter)
	end
	
	local profiler = game.create_profiler()
	profiler.reset()
	method(sample, iterations)
	profiler.stop()
	
	print({'', "Profiled (x", iterations, ") ", friendly_name, ": ", profiler})
end


-- Profiling Commands

commands.add_command('profile_exports', "Profile all the exportable methods in this test", function(data)
	profile(export_serpent, data, "Export Serpent.line")
	profile(export_builtin_json, data, "Export Builtin-JSON")
	profile(export_dkjson, data, "Export External DKJSON")
	profile(export_msgpack, data, "Export MessagePack")
	profile(export_msgpack_b64, data, "Export MessagePack+Base64")
	profile(export_item_with_tags, data, "Export Simple item-with-tags")
	profile(export_persist_item_with_tags, data, "Export Persistent item-with-tags")
end)

commands.add_command('profile_imports', "Profile all the import methods in this test", function(data)
	profile(import_loadstring, data, "Import lua loadstring")
	profile(import_builtin_json, data, "Import Builtin-JSON")
	profile(import_dkjson, data, "Import External DKJSON")
	profile(import_msgpack, data, "Import MessagePack")
	profile(import_msgpack_b64, data, "Import MessagePack+Base64")
	profile(import_item_with_tags, data, "Import Simple item-with-tags")
	profile(import_persist_item_with_tags, data, "Import Persistent item-with-tags")
end)

-- Length of various types.

function datalength(method, params, friendly_name)
	local iterations = 1
	local print = game.print
	if params.player_index > 0 then
		print = game.get_player(params.player_index).print
	end
	
	local result = method(sample, iterations)
	
	print({'', "Length of ", friendly_name, ": ", #result})
end


commands.add_command('len_exports', "Length of all the exportable methods in this test", function(data)
	datalength(export_serpent, data, "Serpent.line")
	datalength(export_builtin_json, data, "Builtin-JSON")
	datalength(export_dkjson, data, "External DKJSON")
	datalength(export_msgpack, data, "MessagePack")
	datalength(export_msgpack_b64, data, "MessagePack+Base64")
	datalength(export_item_with_tags, data, "Item-with-tags")
end)


-- Print examples:

function write_result(method, params, friendly_name)
	local iterations = 1
	local print = game.print
	if params.player_index > 0 then
		print = game.get_player(params.player_index).print
	end
	
	local result = method(sample, iterations)
	
	print({'', "Result of ", friendly_name, " written to script-output/export_results.txt"})
	game.write_file("export_results.txt", friendly_name, true, params.player_index)
	game.write_file("export_results.txt", "\n", true, params.player_index)
	game.write_file("export_results.txt", result, true, params.player_index)
	game.write_file("export_results.txt", "\n", true, params.player_index)
end


commands.add_command('write_exports', "Writes each exportable methods in this to file", function(data)
	write_result(export_serpent, data, "Serpent.line")
	write_result(export_builtin_json, data, "Builtin-JSON")
	write_result(export_dkjson, data, "External DKJSON")
	write_result(export_msgpack, data, "MessagePack")
	write_result(export_msgpack_b64, data, "MessagePack+Base64")
	write_result(export_item_with_tags, data, "Item-with-tags")
end)

